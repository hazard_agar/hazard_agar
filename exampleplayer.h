#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    Move* possibleMoves(Board *game, Side side);
    int calculateHeuristic(Board *inputBoard, Move move, int depth, int moveIsNull);
    Move *doMove(Move *opponentsMove, int msLeft);
private:
    Board *configuration;
    Side color;
    Side oppColor;
    int heurismDepth;
};

#endif
