#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
fprintf(stderr, "initializer\n");
    color=side;
    if(side==WHITE)
        oppColor=BLACK;
    else
        oppColor=WHITE;
    
    //set up board      
    configuration=new Board();

//make another variable for how many times the heuristic should run (how many layers we have in our tree before we can assign a value to the final heurism)-----in the constructor, based on the amount of time that we are given, we should make some kind of formula that figures out how many layers to do on this tree for the given amount of time per move---but that is more complicated 

    heurismDepth=1;
fprintf(stderr, "initializer\n");
    
   

//construct and make it make a move
//list of possible moves, then a heuristic to figure out which move to make based on board state
//so we need attributes of boardState, color, possible moves, and possibly others

//move conditions: can only make a move if you put one of your pegs in a place that turns over an opponent's peg by sandwiching them---after moving, you must update the board state, after the opponent moves, you have to update possible moves, then use heuristic to find next move, then return that move

//might have to free moves and reallocate memory to the moves variable
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}

int ExamplePlayer::calculateHeuristic(Board *inputBoard, Move move, int depth, int moveIsNull)
{return 1243232;
    Side side;
    Board *config=inputBoard->copy();
    if(moveIsNull==0)
    {//0 means false, 1 is true
        side=color;
        if(depth%2==1)
           side=oppColor;
        config->doMove(&move, side);
    }
    depth++;
    int optimalValue;
    int size=64-config->countWhite()-config->countBlack();
    side=color;
    if(depth%2==1)
       side=oppColor;
//odd opponents move---minimum heurism of the moves he makes, even your move----maximum heurism of the moves I make
    if(depth==heurismDepth)
    {
         ///return the heuristic value---------insert the heurism formula here: it should never return 0 or anything less than 0
         return 13222;
    }
    Move *moveList=possibleMoves(config, side);
    if(moveList != NULL)
    {
         int *heuristic= new int[size];
         for(int i=0;i<size;i++)
         {
             if(moveList[i].x==-1)
                  heuristic[i]=0;
             else
                  heuristic[i]=calculateHeuristic(config, moveList[i], depth, 0);
         }
         //min or max part
         if(side==color)
         {
              //my move I pick max heurism
              optimalValue=heuristic[0];
              for(int i=1;i<size;i++)
                  if(optimalValue<heuristic[i])
                      optimalValue=heuristic[i];
         }
         else
         {
              //opponent move---pick min heurism
              optimalValue=heuristic[0];
              for(int i=1;i<size;i++)
                  if(optimalValue>heuristic[i])
                      optimalValue=heuristic[i];
         }
    }
    else
    {
         //takes care of the case when no move can be made (so there are no moves to pick from)
         optimalValue=calculateHeuristic(config, move, depth, 1);
    }
    delete config;
    delete[] moveList;
    return optimalValue;
}
Move* ExamplePlayer::possibleMoves(Board* game, Side side)
{
    Move *movesList= new Move[64-game->countWhite()-game->countBlack()];
    int movesIndex=0;
    for(int x=0;x<8;x++)
       for(int y=0;y<8;y++)
       {
           Move* move= new Move(x,y);
           if(game->checkMove(move, side))
           {
               movesList[movesIndex]=*move;
               movesIndex++;
           }
           delete move;
       }
    if(movesIndex==0)
    { 
       delete[] movesList;
       return NULL;
    }
    return movesList;
}
/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
 

//put in optional argument that tells you how many times we have run this


   
//find a way to make this keep track of time later on--if time is limited then dont go through so many iterations
//later on we have to figure out how to keep track of time and deal with the ms variable


fprintf(stderr, "domove4564564564\n");
    //process opponent's move
    configuration->doMove(opponentsMove, oppColor);
    Move *movesList= new Move[64];//-configuration->countWhite()-configuration->countBlack()
    int movesIndex=0;
 //   int bestHeuristic=0;
    Move *bestMove;
    //generate list of all possible moves
    for(int x=0;x<8;x++)
       for(int y=0;y<8;y++)
       {
           Move *move= new Move(x,y);
      //     Board *mockBoard=configuration->copy();
           if(configuration->checkMove(move, color))
           {
configuration->doMove(move, color);
return move;
               movesList[movesIndex]=*move;
 //              heuristic[movesIndex]=21;
//calculateHeuristic(mockBoard, *move, 0, 0);
               movesIndex++;
           }
           else
           {
               delete move;
           }
  //         delete mockBoard;
       }
fprintf(stderr, "%d\n", movesIndex);
    if(movesIndex==0)
       return NULL;
bestMove=&movesList[0];
configuration->doMove(bestMove, color);
return bestMove;
 /*   for(int i=0;i<movesIndex;i++)
    {
        if(heuristic[i]>bestHeuristic)
        {
            bestHeuristic=heuristic[i];
            bestMove=&movesList[i];
        }   
    }  */
//                 fprintf(stderr, "%d\n", bestHeuristic);fprintf(stderr, "%d\n", bestMove->x);fprintf(stderr, "%d\n", bestMove->y);
//    configuration->doMove(bestMove, color);
//   delete[] movesList;
 //   delete[] heuristic;
 //   return bestMove;
}
