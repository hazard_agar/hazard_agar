Christopher Hazard/ Anish Agarwal

1. Both of us worked on each part of the assignment together, taking turns typing, and checking over what we each wrote.

2. In the first week, we encountered a bug in the code that had something to our arrays in our doMove function. Despite spending a lot of time trying to debug this, we couldn't find the source of the error, which prevented us from trying our proposed strategy (discussed below). However, we were able to find a way around this error, but we had to use a very simple strategy that basically equated to picking a random move. 
Had we not encountered this error, which we were unable to fix, we would have implemented the following strategy:

1. For each possible move during a given turn, we would have called a function that implemented the minimax algorithm and returned an integer value (the heuristic). Then we would pick the maximum possible heuristic value and make that move.

2. We were planning on having the minimax algorithm search down to a depth depending on the amount of time alloted per move. To calibrate this, we would have estimated the average amount of time the computer spent calculating a given move, and adjusted the computer's estimation of how much time the minimax calculation would take at a certain depth. (It would have gone as far as possible without exceeding the time limit). As a failsafe, we were also planning on having it return the highest heuristic so far if it underestimated how much time it would take to calculate.

3. As for the heuristic functions, we were planning on starting off with finding the number of your pieces minus opponent's pieces that would be on the board after a proposed sequence of moves. From there, we would have tried assigning higher values to pieces in more advantageous places such as the corners and edges of the board. We also would have tried the mobility heuristic (maxizing our possible moves for our next turn while minimizing our opponent's options), and tried the Frontier squares heurism. Eventually, we would have figured out how we should weight these values to obtain optimal results.
